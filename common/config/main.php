<?php

use yii\rbac\DbManager;
use yii\caching\FileCache;

return [
    'aliases'    => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'name'       => 'CallUS Portal',
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'formatter' => [
            'dateFormat'        => 'dd-MM-yyyy',
            'datetimeFormat'    => 'dd-MM-yyyy HH:mm:ss',
            'decimalSeparator'  => ',',
            'thousandSeparator' => '.',
            'currencyCode'      => 'VND',
            'defaultTimeZone'   => 'Asia/Ho_Chi_Minh',
            'timeZone'          => 'Asia/Ho_Chi_Minh',
        ],
        'authManager' => [
            'class' => DbManager::class,
        ],
        'queue'     => [
            'class'  => \yii\queue\sync\Queue::class,
            'handle' => true,
            'as log' => \yii\queue\LogBehavior::class,
            // 'path'   => '@root/queue',
        ],
        'i18n'         => [
            'translations' => [
                'common*'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@rootPath/messages',
                ],
                'frontend*' => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@rootPath/messages',
                ],
                'backend*'  => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@rootPath/messages',
                ],
                'app*'      => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@rootPath/messages',
                ],
                'menu*'     => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@rootPath/messages',
                ],
                '*'         => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@rootPath/messages',
                ],
            ],
        ],
    ],
];
