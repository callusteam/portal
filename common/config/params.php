<?php
return [
    'adminEmail'                    => 'admin@example.com',
    'supportEmail'                  => 'support@example.com',
    'senderEmail'                   => 'noreply@example.com',
    'senderName'                    => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'uploadsDirectory'              => dirname(__DIR__, 2) . '/uploads',
    'avatarUploadPath'              => dirname(__DIR__, 2) . '/uploads/avatar',
    'tempUploadPath'                => dirname(__DIR__, 2) . '/uploads/tmp',
    'uploadMaxSize'                 => 2, // Megabyte
];
