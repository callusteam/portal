<?php

namespace common\models;

use common\models\setting\ImportItAsset;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\BaseConsole;
use function date;
use function file_exists;
use function implode;
use function is_numeric;
use function mkdir;
use function pathinfo;
use function serialize;
use function str_replace;
use function strtolower;
use const DIRECTORY_SEPARATOR;
use const PATHINFO_EXTENSION;
use const PHP_EOL;

/**
 * This is the model class for table "{{%import}}".
 *
 * @property int    $id_import
 * @property string $file_name
 * @property string $file_path
 * @property string $mapping
 * @property int    $type
 * @property string $log
 * @property int    $id_user
 * @property int    $status
 * @property int    $created_at
 * @property int    $updated_at
 *
 * @property User   $user
 * @property mixed  $fileDownload
 * @property string $statusDisplay
 * @property string $typeDisplay
 *
 */
class Import extends \yii\db\ActiveRecord
{
    const STATUS_DELETED   = 0;
    const STATUS_NEW       = 1;
    const STATUS_IMPORTING = 2;
    const STATUS_SUCCESS   = 3;
    const STATUS_FAIL      = 4;
    const STATUS_WAITING   = 5;
    const STATUS_ACTIVE    = 10;
    
    //Sample file
    const SAMPLE_MCK = 'CALLUS MCK FUNNEL updated to 15.07.2019.xlsx';
    
    //Type
    const TYPE_MCK = 1;
    
    const IMPORT_UPLOAD_DIR = 'import';
    const IMPORT_SAMPLE_DIR = 'sample';
    
    private $mappings = [];
    
    //MCK COLUMNS
    const COL_MCK_USERNAME   = 'username';
    const COL_MCK_ALLOCATED  = 'allocated';
    const COL_MCK_CALLED     = 'called';
    const COL_MCK_CONTACTED  = 'contacted';
    const COL_MCK_INTERESTED = 'interested';
    const COL_MCK_MEETING    = 'meeting';
    const COL_MCK_APP        = 'app';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%import}}';
    }
    
    /**
     * @return \common\models\User|\yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id_user' => 'id_user']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mapping', 'log'], 'string'],
            [['type', 'id_user', 'status', 'created_at', 'updated_at'], 'integer'],
            [['file_path'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_NEW],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_import'  => Yii::t('common', 'Id Import'),
            'file_name'  => Yii::t('common', 'File Name'),
            'file_path'  => Yii::t('common', 'File Path'),
            'mapping'    => Yii::t('common', 'Mapping'),
            'type'       => Yii::t('common', 'Type'),
            'log'        => Yii::t('common', 'Log'),
            'id_user'    => Yii::t('common', 'User'),
            'status'     => Yii::t('common', 'Status'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }
    
    
    // public function delete()
    // {
    //     $this->status = self::STATUS_DELETED;
    //     return parent::save();
    // }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
    
    public static function typeLabel($type)
    {
        switch ($type) {
            case self::TYPE_MCK:
                return Yii::t('common', 'MCK Data');
                break;
            default:
                return '';
        }
    }
    
    public static function typeLabels()
    {
        return [
            self::TYPE_MCK => self::typeLabel(self::TYPE_MCK),
        ];
    }
    
    public static function statusLabel($status)
    {
        switch ($status) {
            case self::STATUS_NEW:
                return Yii::t('common', 'New');
                break;
            case self::STATUS_DELETED:
                return Yii::t('common', 'Deleted');
                break;
            case self::STATUS_WAITING:
                return Yii::t('common', 'Waiting');
                break;
            case self::STATUS_IMPORTING:
                return Yii::t('common', 'Importing');
                break;
            case self::STATUS_SUCCESS:
                return Yii::t('common', 'Success');
                break;
            case self::STATUS_FAIL:
                return Yii::t('common', 'Fail');
                break;
            default:
                return '';
        }
    }
    
    public static function statusLabels()
    {
        return [
            self::STATUS_DELETED   => self::statusLabel(self::STATUS_DELETED),
            self::STATUS_NEW       => self::statusLabel(self::STATUS_NEW),
            self::STATUS_WAITING   => self::statusLabel(self::STATUS_WAITING),
            self::STATUS_IMPORTING => self::statusLabel(self::STATUS_IMPORTING),
            self::STATUS_SUCCESS   => self::statusLabel(self::STATUS_SUCCESS),
            self::STATUS_FAIL      => self::statusLabel(self::STATUS_FAIL),
        ];
    }
    
    public static function sampleFile($type)
    {
        switch ($type) {
            case self::TYPE_MCK:
                return implode(DIRECTORY_SEPARATOR, [Yii::$app->params['uploadsDirectory'], self::IMPORT_SAMPLE_DIR, self::SAMPLE_MCK]);
                break;
            default:
                return false;
        }
    }
    
    public function setMapping()
    {
        if ($this->type == self::TYPE_MCK) {
            $this->setMappingMck();
        }
    }
    
    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->isNewRecord) {
            $this->setMapping();
            $this->mapping = serialize($this->mappings);
            $this->id_user = Yii::$app->user->getIdentity() ? Yii::$app->user->getIdentity()->getId() : 0;
        }
        
        return parent::save($runValidation, $attributeNames);
    }
    
    /**
     *
     */
    public function ProcessImport()
    {
        if ($this->type == self::TYPE_MCK) {
            $this->processImportMck();
        }
    }
    
    
    /**
     * @return string
     */
    public function getFileDownload()
    {
        return implode(DIRECTORY_SEPARATOR, [Yii::$app->params['uploadsDirectory'], self::IMPORT_UPLOAD_DIR, $this->file_path]);
    }
    
    /**
     * @param $file
     *
     * @return string
     */
    public static function savePath($file)
    {
        $dir = implode(DIRECTORY_SEPARATOR, [
            Yii::$app->params['uploadsDirectory'],
            self::IMPORT_UPLOAD_DIR,
            'files',
            date('Y/m/d'),
        ]);
        if (!file_exists($dir) && !mkdir($dir, 0777, true) && !is_dir($dir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }
        return $dir;
    }
    
    /** MCK */
    
    /**
     *
     */
    private function setMappingMck()
    {
        //
        $this->mappings = [
            'firstRecord'       => 3,
            'update_exist_data' => false,
            'col_map'           => [
                self::COL_MCK_USERNAME   => 'A',
                self::COL_MCK_ALLOCATED  => 'D',
                self::COL_MCK_CALLED     => 'E',
                self::COL_MCK_CONTACTED  => 'F',
                self::COL_MCK_INTERESTED => 'G',
                self::COL_MCK_MEETING    => 'H',
                self::COL_MCK_APP        => 'I',
            ],
        ];
    }
    
    /**
     *
     */
    private function processImportMck()
    {
        //
        try {
            $path = implode(DIRECTORY_SEPARATOR, [Yii::$app->params['uploadsDirectory'], self::IMPORT_UPLOAD_DIR, 'files']);
            $file = $path . DIRECTORY_SEPARATOR . $this->file_path;
            $ext  = pathinfo($file, PATHINFO_EXTENSION);
            BaseConsole::output($file);
            BaseConsole::output("File -->  $file");
            if (strtolower($ext) == 'xls') {
                $objReader = IOFactory::createReader('Xls');
            } elseif (strtolower($ext) == 'xlsx') {
                $objReader = IOFactory::createReader('Xlsx');
            } elseif (strtolower($ext) == 'csv') {
                $objReader = IOFactory::createReader('Csv');
            } else {
                $this->status = self::STATUS_FAIL;
                $this->log    = 'Invalid file format';
                $this->save();
                
                return false;
            }
            
            $columns_config = unserialize($this->mapping);
            // $update_exist_data = $columns_config['update_exist_data'];
            $columns_map = $columns_config['col_map'];
            
            $objReader->setReadDataOnly(true);
            try {
                $objPHPExcel = $objReader->load($file);
            } catch (Exception $ex) {
                $error_msg    = str_replace($file, $this->file_path, $ex->getMessage());
                $this->status = self::STATUS_FAIL;
                $this->log    = 'Import Error :' . $error_msg;
                $this->save();
                
                return false;
            }
            
            $objWorksheet = $objPHPExcel->getSheet(2);
            
            $highestRow    = $objWorksheet->getHighestRow(); // e.g. 10
            $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
            
            echo "Total row : $highestRow" . PHP_EOL;
            echo "Total columns : $highestColumn" . PHP_EOL;
            Yii::debug($columns_map);
            
            $count = 0;
            for ($row = $columns_config['firstRecord']; $row <= $highestRow; $row++) {
                try {
                    foreach ($columns_map as $colName => $col) {
                        $colIndex = is_numeric($col) ? $col : Coordinate::columnIndexFromString($col);
                        $column   = $objWorksheet->getCellByColumnAndRow($colIndex, $row);
                        if ($column) {
                            $colValue = $column->getValue();
                            echo $colValue . PHP_EOL;
                        }
                    }
                } catch (\Exception $ex) {
                    $this->status = self::STATUS_FAIL;
                    $this->log    = 'Error: ' . $ex->getMessage();
                    $this->save();
                    
                    return false;
                }
            }
            $this->status = self::STATUS_SUCCESS;
            $this->log    = "Import Success $count records";
            $this->save();
            
            return true;
        } catch (\Exception $ex) {
            $this->status = Import::STATUS_FAIL;
            $this->log    = 'Error: ' . $ex->getMessage();
            $this->save();
            
            return false;
        }
    }
    /** END MCK */
    
    /**
     * @return string
     */
    public function getStatusDisplay(): string
    {
        return self::statusLabel($this->status);
    }
    
    /**
     * @return string
     */
    public function getTypeDisplay(): string
    {
        return self::typeLabel($this->type);
    }
    

}