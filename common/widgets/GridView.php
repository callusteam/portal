<?php

namespace common\widgets;


class GridView extends \kartik\grid\GridView
{
    // public $striped = false;
    // public $bordered = false;
    public $responsive = false;
    public $resizableColumns = false;
    public $export = false;
}