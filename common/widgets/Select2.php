<?php
namespace common\widgets;

class Select2 extends \kartik\select2\Select2
{
    public $theme = \kartik\widgets\Select2::THEME_BOOTSTRAP;
}