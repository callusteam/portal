<?php
/**
 *
 * User: ThangDang
 * Date: 2019-07-11
 * Time: 09:24
 *
 */

namespace common\widgets;


use function array_merge;

class FileInput extends \kartik\widgets\FileInput
{
    public function init()
    {
        parent::init();
        $this->pluginOptions = array_merge([
            'showPreview' => false,
            'showRemove'  => false,
            'showUpload'  => false,
        ], $this->pluginOptions);
    }
}