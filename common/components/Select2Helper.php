<?php
/**
 *
 * User: ThangDang
 * Date: 2019-07-03
 * Time: 22:13
 *
 */

namespace backend\components;


class Select2Helper
{
    public static function formatResult()
    {
        $resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 20) < data.total_count
        }
    };
}
JS;
        return $resultsJs;
    }
}