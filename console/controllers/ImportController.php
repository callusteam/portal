<?php
/**
 *
 * User: ThangDang
 * Date: 6/22/18
 * Time: 16:50
 *
 */

namespace console\controllers;


use common\models\Import;
use common\models\ImportUser;
use Exception;
use yii\console\Controller;
use yii\helpers\BaseConsole;
use const YII_ENV_DEV;

class ImportController extends Controller
{
    /**
     * @param $id integer
     */
    public function actionIndex($id)
    {
        $import = Import::findOne($id);
        if($import)
        {
            if(YII_ENV_DEV)
            {
                $import->status = Import::STATUS_WAITING;
                $import->log    = '';
            }
            
            if($import->status == Import::STATUS_WAITING)
            {
                try
                {
                    BaseConsole::output('Begin Import');
                    //Switch status
                    $import->status = Import::STATUS_IMPORTING;
                    $import->save();
                    $import->refresh();
                    $import->ProcessImport();
                    BaseConsole::output('Import Completed');
                }catch(Exception $exception)
                {
                    BaseConsole::error($exception->getMessage());
                }
            }
            else
            {
                BaseConsole::error('Invalid Status');
            }
        }
        else
        {
            BaseConsole::error('Import not found');
        }
    }
}