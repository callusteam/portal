<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%import}}`.
 */
class m190805_025548_create_import_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%import}}', [
            'id_import'  => $this->primaryKey(),
            'file_name'  => $this->string(),
            'file_path'  => $this->string(),
            'mapping'    => $this->text(),
            'type'       => $this->smallInteger(2),
            'log'        => $this->text(),
            'id_user'    => $this->integer(),
            'status'     => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%import}}');
    }
}
