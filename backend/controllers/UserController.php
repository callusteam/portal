<?php

namespace backend\controllers;

use backend\models\form\Avatar;
use backend\models\form\PasswordResetRequest;
use backend\models\form\UserUpdate;
use backend\models\form\SignUp;
use Yii;
use common\models\User;
use backend\models\search\UserSearch;
use yii\base\UserException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index',[
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Displays a single User model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view',[
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Signup();
        
        if ($model->load(Yii::$app->request->post()) && $user = $model->signup()) {
            return $this->redirect(['view','id' => $user->id_user]);
        }
        
        return $this->render('create',[
            'model' => $model,
        ]);
    }
    
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = new UserUpdate();
        $user  = $this->findModel($id);
        $model->loadModel($user);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view','id' => $model->id]);
        }
        
        return $this->render('update',[
            'model' => $model,
        ]);
        
    }
    
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException(Yii::t('backend','The requested page does not exist.'));
    }
    
    /**
     * Update a User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionChangeAvatar($id)
    {
        $model = new Avatar();
        $user  = $this->findModel($id);
        $model->loadModel($user);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view','id' => $user->id_user]);
        }else {
            return $this->render('change-avatar',[
                'model' => $model,
            ]);
        }
    }
    
    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionResetPassword($id)
    {
        $model                       = $this->findModel($id);
        $passwordRequestReset        = new PasswordResetRequest();
        $passwordRequestReset->email = $model->email;
        
        if ($passwordRequestReset->sendEmail()) {
            Yii::$app->getSession()->setFlash('success',Yii::t('backend','Password reset link sent to user.'));
        }else {
            Yii::$app->getSession()->setFlash('error',Yii::t('backend','Sorry, we are unable to reset password for email provided.'));
        }
        
        
        return $this->redirect(['view','id' => $model->id_user]);
    }
    
    /**
     * @param      $id
     * @param      $id_parent
     *
     * @param null $callbackURL
     *
     * @return \yii\web\Response
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRestore($id,$id_parent = null,$callbackURL = null)
    {
        /* @var $user User */
        $user = $this->findModel($id);
        if ($user->status == User::STATUS_DELETED && Yii::$app->user->id != $user->id_user) {
            $user->status = User::STATUS_ACTIVE;
            if ($user->save()) {
                if ($callbackURL) {
                    return $this->redirect($callbackURL);
                }
                
                if ($id_parent != null) {
                    return $this->redirect(['view','id' => $id_parent]);
                }
                
                return $this->redirect(['view','id' => $id]);
            }else {
                $errors = $user->firstErrors;
                throw new UserException(reset($errors));
            }
        }
        
        return $this->redirect(['index']);
    }
    
    
    /**
     * @param      $id
     * @param      $id_parent
     *
     * @param null $callbackURL
     *
     * @return \yii\web\Response
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id,$id_parent = null,$callbackURL = null)
    {
        /* @var $user User */
        $user = $this->findModel($id);
        if ($user->status == User::STATUS_ACTIVE && Yii::$app->user->id != $user->id_user) {
            $user->status = User::STATUS_DELETED;
            if ($user->save()) {
                if ($callbackURL) {
                    return $this->redirect($callbackURL);
                }
                
                if ($id_parent != null) {
                    return $this->redirect(['view','id' => $id_parent]);
                }
                
                return $this->redirect(['view','id' => $id]);
            }else {
                $errors = $user->firstErrors;
                throw new UserException(reset($errors));
            }
        }
        
        return $this->redirect(['index']);
    }
}
