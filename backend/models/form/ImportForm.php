<?php
/**
 *
 * User: ThangDang
 * Date: 2019-08-05
 * Time: 11:35
 *
 */

namespace backend\models\form;


use backend\models\Import;
use RuntimeException;
use Yii;
use yii\web\UploadedFile;
use function file_exists;
use function implode;
use function is_dir;
use function mkdir;
use function sprintf;
use function time;
use function unlink;
use const DIRECTORY_SEPARATOR;

class ImportForm extends Import
{
    public $fileUpload;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                'fileUpload',
                'file',
                'skipOnEmpty' => false,
                'extensions'  => ['csv', 'xls', 'xlsx'],
                'maxSize'     => Yii::$app->params['uploadMaxSize'] * 1024 * 1024,
            ],
            [['type',], 'integer'],
            [['type',], 'required'],
            ['status', 'default', 'value' => self::STATUS_NEW],
        ];
    }
    
    public function save($runValidation = true, $attributeNames = null)
    {
        $this->fileUpload = UploadedFile::getInstance($this, 'fileUpload');
        if (!$this->validate()) {
            $this->addError('file', Yii::t('backend', 'Upload file error'));
            
            return false;
        }
        $upload_dir = implode(DIRECTORY_SEPARATOR, [Yii::$app->params['uploadsDirectory'], \common\models\Import::IMPORT_UPLOAD_DIR,'files']);
        
        if (!file_exists($upload_dir) && !mkdir($upload_dir,0777,true) && !is_dir($upload_dir)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $upload_dir));
        }
        $file_name = Yii::$app->security->generateRandomString(10) . '_' . time() . '.' . $this->fileUpload->extension;
        $file_path = $upload_dir . DIRECTORY_SEPARATOR . $file_name;
        Yii::debug($file_path);
        if (!$this->fileUpload->saveAs($file_path)) {
            $this->addError('fileUpload', Yii::t('backend', 'Upload file error'));
            
            return false;
        }
        
        $this->file_path = $file_name;
        $this->file_name = $this->fileUpload->name;
        if (!$result = parent::save(false)) {
            @unlink($file_path);
        }
        
        return $result;
    }
    
}