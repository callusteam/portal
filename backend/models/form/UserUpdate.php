<?php

namespace backend\models\form;

use common\models\User;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class UserUpdate extends Model
{
    public $id;
    public $full_name;
    public $email;
    public $status;
    public $new_password;
    public $retype_password;
    public $roles = [];
    
    /**
     * @var User
     */
    public $user;
    
    
    public function loadModel(User $user)
    {
        $this->user      = $user;
        $this->id        = $user->id_user;
        $this->full_name = $user->full_name;
        $this->email     = $user->email;
        $this->status    = $user->status;
        
        $authManager = Yii::$app->authManager;
        if ($authManager !== null) {
            $roles = $authManager->getRolesByUser($user->id_user);
            if ($roles !== null && !empty($roles) && count($roles) > 0) {
                $this->roles = ArrayHelper::getColumn(Yii::$app->authManager->getRolesByUser($user->id_user), 'name');
            }
        }
        
        
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'full_name'       => Yii::t('backend', 'Full Name'),
            'email'           => Yii::t('backend', 'Email'),
            'old_password'    => Yii::t('backend', 'Old Password'),
            'new_password'    => Yii::t('backend', 'New Password'),
            'retype_password' => Yii::t('backend', 'Retype Password'),
            'roles'           => Yii::t('backend', 'Roles'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => 'app\models\User',
                'message'     => Yii::t('backend', 'This email address has already been taken.'),
                'when'        => function (
                    UserUpdate $model
                ) {
                    return $model->email != $this->user->email; // or other function for get current username
                },
            ],
            
            ['new_password', 'string', 'min' => 5],
            [['retype_password'], 'compare', 'compareAttribute' => 'new_password'],
            
            
            ['full_name', 'string', 'min' => 2, 'max' => 255],
            ['full_name', 'required'],
            [['status'], 'integer'],
            [['roles'], 'safe'],
        ];
    }
    
    /**
     * @return null|User
     */
    public function save()
    {
        if ($this->validate() && $this->user != null) {
            /**
             * @var $user User
             */
            $user            = $this->user;
            $user->email     = $this->email;
            $user->full_name = $this->full_name;
            // $user->roles     = $this->roles;
            if (\Yii::$app->user->id != $user->id_user) {
                $user->status = $this->status;
            }
            
            try {
                if (isset($this->new_password) && !empty($this->new_password) && isset($this->retype_password) && !empty($this->retype_password)) {
                    $user->setPassword($this->new_password);
                }
                $user->generateAuthKey();
            } catch (Exception $e) {
            }
            if ($user->save()) {
                return $user;
            }
        }
        
        return null;
    }
}