<?php

namespace backend\models\form;

use common\models\User;
use Yii;
use yii\base\Exception;
use yii\base\Model;

class SignUp extends Model
{
    public $username;
    public $email;
    //public $password;
    //public $retypePassword;
    public $full_name;
    public $status;
    
    public $should_send_activation_mail;
    public $roles = [];
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['should_send_activation_mail', 'boolean'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => User::class, 'message' => Yii::t('backend', 'This username has already been taken.')],
            ['username', 'string', 'min' => 2, 'max' => 255],
            
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::class, 'message' => Yii::t('backend', 'This email address has already been taken.')],
            
            ['full_name', 'string', 'min' => 2, 'max' => 255],
            ['full_name', 'required'],
            [['status'], 'integer'],
            [['roles'], 'safe'],
        ];
    }
    
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user            = new User();
            $user->username  = $this->username;
            $user->email     = $this->email;
            $user->full_name = $this->full_name;
            $user->status    = $this->status;
            $user->roles     = $this->roles;
            try {
                $password = Yii::$app->getSecurity()->generateRandomString(8);
                $user->setPassword($password);
                $user->generateAuthKey();
            } catch (Exception $e) {
            };
            if ($user->save()) {
                if ($this->should_send_activation_mail == true) {
                    $this->sendEmail();
                }
                
                return $user;
            }
        }
        
        return null;
    }
    
    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email'  => $this->email,
        ]);
        
        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                try {
                    $user->generatePasswordResetToken();
                } catch (Exception $e) {
                }
            }
            
            if ($user->save()) {
                return Yii::$app->mailer->compose(['html' => '@backend/mail/signup-html', 'text' => '@backend/mail/signup-text'], ['user' => $user])
                    ->setFrom([Yii::$app->params['noReplyEmail'] => Yii::$app->params['appName']])
                    ->setTo($this->email)
                    ->setSubject(Yii::t('backend', '[{name}] Password setting for {appName}', [
                        'name'    => Yii::$app->params['appName'],
                        'appName' => Yii::$app->params['appName'],
                    ]))
                    ->send();
            }
        }
        
        return false;
    }
}