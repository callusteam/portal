<?php

namespace backend\models\form;

use common\models\User;
use Exception;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use yii\base\Model;
use yii\helpers\Json;
use yii\imagine\Image;
use yii\web\UploadedFile;
use function unlink;

class Avatar extends Model
{
    /**
     * @var UploadedFile
     */
    public $image_file;
    public $user;
    
    public $crop_info;
    
    public function loadModel(User $user)
    {
        $this->user = $user;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image_file' => Yii::t('backend', 'Image File'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['image_file'],
                'image',
                'skipOnEmpty' => true,
                'extensions'  => ['jpg', 'jpeg', 'png', 'gif'],
                'mimeTypes'   => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'],
                'maxSize'     => Yii::$app->params['uploadMaxSize'] * 1024 * 1024,
            ],
            ['crop_info', 'safe'],
        ];
    }
    
    public function save()
    {
        try {
            if ($this->user != null && $this->validate()) {
                /**
                 * @var $user User
                 */
                $user             = $this->user;
                $this->image_file = UploadedFile::getInstance($this, 'image_file');
                if ($this->image_file) {
                    if (!file_exists(Yii::$app->params['avatarUploadPath']) && !mkdir($concurrentDirectory = Yii::$app->params['avatarUploadPath']) && !is_dir($concurrentDirectory)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created',
                            $concurrentDirectory));
                    }
                    
                    $upload_path = Yii::$app->params['avatarUploadPath'];
                    $temp_path   = Yii::$app->params['tempUploadPath'];
                    
                    Yii::debug($upload_path);
                    Yii::debug($temp_path);
                    
                    if (!file_exists($upload_path) && !mkdir($upload_path) && !is_dir($upload_path)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created', $upload_path));
                    }
                    
                    if (!file_exists($temp_path) && !mkdir($temp_path) && !is_dir($temp_path)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created', $temp_path));
                    }
                    
                    $temp_file_path = $temp_path . DIRECTORY_SEPARATOR . 'temp_' . $this->image_file->baseName . '.' . $this->image_file->extension;
                    Yii::debug($temp_file_path);
                    
                    if ($this->image_file->saveAs($temp_file_path)) {
                        // open image
                        $image = Image::getImagine()->open($temp_file_path);
                        
                        $original_file_name   = $user->id_user . '_original';
                        $original_upload_file = $original_file_name . '.png';
                        $original_path        = $upload_path . DIRECTORY_SEPARATOR . $original_upload_file;
                        $image->save($original_path, ['quality' => 90]);
                        
                        $file_name   = $user->id_user;
                        $upload_file = $file_name . '.png';
                        $path        = $upload_path . DIRECTORY_SEPARATOR . $upload_file;
                        // rendering information about crop of ONE option
                        if ($this->crop_info != null && count(Json::decode($this->crop_info)) > 0) {
                            $cropInfo            = Json::decode($this->crop_info)[0];
                            $cropInfo['dWidth']  = (int)$cropInfo['dWidth']; //new width image
                            $cropInfo['dHeight'] = (int)$cropInfo['dHeight']; //new height image
                            $cropInfo['x']       = $cropInfo['x']; //begin position of frame crop by X
                            $cropInfo['y']       = $cropInfo['y']; //begin position of frame crop by Y
                            
                            
                            //saving thumbnail
                            $newSizeThumb   = new Box($cropInfo['dWidth'], $cropInfo['dHeight']);
                            $cropSizeThumb  = new Box(200, 200); //frame size of crop
                            $cropPointThumb = new Point($cropInfo['x'], $cropInfo['y']);
                            
                            $image->resize($newSizeThumb)
                                ->crop($cropPointThumb, $cropSizeThumb)
                                ->save($path, ['quality' => 90]);
                            
                            if ($image !== null) {
                                if ($temp_file_path) {
                                    unlink($temp_file_path);
                                }
                                
                                return true;
                            }
                        } else {
                            $this->addError('image_file', Yii::t('backend', 'Please select crop area.'));
                        }
                    } else {
                        $this->addError('image_file', Yii::t('backend', 'Failed to save temp image.'));
                    }
                } else {
                    $this->addError('image_file', Yii::t('backend', 'Please select an image.'));
                }
            }
        } catch (Exception $exception) {
            $this->addError('image_file', Yii::t('backend', 'Failed to save image. Please try again.'));
            Yii::error($exception);
        }
        
        return false;
    }
}