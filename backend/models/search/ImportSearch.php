<?php

namespace backend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Import;

/**
 * ImportSearch represents the model behind the search form of `backend\models\Import`.
 */
class ImportSearch extends Import
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_import', 'type', 'id_user', 'status', 'created_at', 'updated_at'], 'integer'],
            [['file_name', 'file_path', 'mapping', 'log'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Import::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_import' => $this->id_import,
            'type' => $this->type,
            'id_user' => $this->id_user,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'file_name', $this->file_name])
            ->andFilterWhere(['ilike', 'file_path', $this->file_path])
            ->andFilterWhere(['ilike', 'mapping', $this->mapping])
            ->andFilterWhere(['ilike', 'log', $this->log]);

        return $dataProvider;
    }
}
