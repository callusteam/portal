<?php

use common\components\LanguageSelector;
use common\models\User;
use mdm\admin\controllers\AssignmentController;
use mdm\admin\Module;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'call-us-portal-backend',
    'name'                => 'Backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => [
        'log',
    ],
    'modules'             => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'admin'    => [
            'class'         => Module::class,
            'mainLayout'    => '@backend/views/layouts/main.php',
            'controllerMap' => [
                'assignment' => [
                    'class'         => AssignmentController::class,
                    'userClassName' => User::class,
                    'idField'       => 'id_user',
                    'usernameField' => 'username',
                    'fullnameField' => 'full_name',
                ],
            ],
            'menus'         => [
                'assignment' => null,
                //'route'      => null, // disable menu route
            ],
        
        ],
    ],
    'components'          => [
        'request'      => [
            'csrfParam' => '_csrf-backend',
        ],
        'user'         => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session'      => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
            ],
        ],
    ],
    'params'              => $params,
];
