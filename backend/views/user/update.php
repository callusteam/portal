<?php

/* @var $this yii\web\View */
/* @var $model \backend\models\form\UserUpdate */

$this->title                   = Yii::t('backend', 'Update User: ' . $model->user->id_user, [
    'nameAttribute' => '' . $model->user->id_user,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user->id_user, 'url' => ['view', 'id' => $model->user->id_user]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="user-update">
    
    <?= $this->render('_form-update', [
        'model' => $model,
    ]) ?>

</div>
