<?php

use common\models\User;
use common\widgets\GridView;
use kartik\grid\ActionColumn;
use kartik\grid\SerialColumn;
use mdm\admin\components\Helper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'        => [
            'type'    => GridView::TYPE_PRIMARY,
            'heading' => '<i class="glyphicon glyphicon-list"></i> ' . $this->title,
        ],
        'toolbar'      => [
            'content' =>
                Helper::checkRoute('user/create') ?
                    Html::a('<i class="fa fa-plus" aria-hidden="true"></i>', ['create'], ['class' => 'btn btn-success btn-sm']) : '',
        ],
        'columns'      => [
            
            ['class' => SerialColumn::class],
            'username',
            'full_name',
            'email:email',
            'created_at:date',
            [
                'attribute' => 'status',
                'value'     => function ($model) {
                    return $model->status == User::STATUS_ACTIVE ? '<span class="badge bg-green">' . User::statusLabel($model->status) . '</span>' : '<span class="badge bg-red">' . User::statusLabel($model->status) . '</span>';
                },
                'format'    => 'raw',
            ],
            [
                'class'    => ActionColumn::class,
                'template' => Helper::filterActionColumn(['view', 'update', 'restore', 'delete']),
                'buttons'  => [
                    'restore' => function ($url, $model) {
                        if ($model->status == User::STATUS_ACTIVE || Yii::$app->user->id == $model->id_user) {
                            return '';
                        }
                        $options = [
                            'title'        => Yii::t('backend', 'Restore'),
                            'aria-label'   => Yii::t('backend', 'Restore'),
                            'data-confirm' => Yii::t('backend', 'Are you sure you want to restore this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        
                        return Html::a('<span class="glyphicon glyphicon-ok text-success"></span>', [
                            '/user/restore',
                            'id'          => $model->id_user,
                            'callbackURL' => Yii::$app->request->url,
                        ], $options);
                    },
                    'delete'  => function ($url, $model) {
                        if ($model->status == User::STATUS_DELETED || Yii::$app->user->id == $model->id_user) {
                            return '';
                        }
                        $options = [
                            'title'        => Yii::t('backend', 'Delete'),
                            'aria-label'   => Yii::t('backend', 'Delete'),
                            'data-confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ];
                        
                        return Html::a('<span class="glyphicon glyphicon-remove text-danger"></span>', [
                            '/user/delete',
                            'id'          => $model->id_user,
                            'callbackURL' => Yii::$app->request->url,
                        ], $options);
                    },
                    
                    'update' => function ($url, $model) {
                        if (Yii::$app->user->id == $model->id_user) {
                            return '';
                        }
                        $options = [
                            'title'      => Yii::t('backend', 'Update'),
                            'aria-label' => Yii::t('backend', 'Update'),
                        ];
                        
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/user/update', 'id' => $model->id_user],
                            $options);
                    },
                ],
            ],
        ],
    ]);
    ?>
</div>
