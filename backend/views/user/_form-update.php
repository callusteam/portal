<?php

use common\models\User;
use common\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\form\UserUpdate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin(['class' => 'form']); ?>
            
            
            <?= $form->field($model, 'full_name')->textInput() ?>
            
            <?= $form->field($model, 'email')->textInput() ?>
            
            <?php echo $form->field($model, 'roles')->widget(Select2::class, [
                'data'          => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'name'),
                'options'       => [
                    'multiple'    => true,
                    'placeholder' => Yii::t('backend', 'Select'),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label(Yii::t('backend', 'Roles')) ?>
            
            
            <?= $form->field($model, 'status')->dropDownList(User::statusLabels()) ?>

            <div class="panel panel-warning">
                <div class="panel-heading"><h4 class="panel-title"><?= Yii::t('backend', 'Change password') ?></h4></div>
                <div class="panel-body">
                    <?= $form->field($model, 'new_password')->passwordInput([
                        'maxlength'    => true,
                        'autocomplete' => 'new-password',
                    ]) ?>
                    
                    <?= $form->field($model, 'retype_password')->passwordInput([
                        'maxlength'    => true,
                        'autocomplete' => 'new-password',
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Update'), [
                    'class' => 'btn btn-primary btn-submit',
                    'title' => Yii::t('backend', 'Update'),
                ]) ?>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
