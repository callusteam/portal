<?php

use common\models\User;
use mdm\admin\components\Helper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title                   = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$controllerId = $this->context->uniqueId . '/';
?>
<div class="user-view">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <?= Yii::t('backend', 'Information') ?>
                </div>
                <div class="panel-body">
                    <div class="widget-user-header bg-teal">
                        <div class="widget-user-image">
                            <?= Html::img($model->getAvatar(), ['class' => 'img-circle img-responsive']) ?>
                        </div>
                        <h3 class="widget-user-username"><?= $this->title ?></h3>
                        <h5 class="widget-user-desc"><?= $model->username ?></h5>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <?php if (Helper::checkRoute($controllerId . 'change-avatar')): ?>
                                <li>
                                    <?= Html::a(Yii::t('backend',
                                            'Change avatar') . '<span class="pull-right badge bg-blue"><i class="fa fa-camera"></i></span>', [
                                        'change-avatar',
                                        'id' => $model->id_user,
                                    ], [
                                        'title' => Yii::t('backend', 'Change avatar'),
                                    ]) ?>
                                </li>
                            <?php endif; ?>
                            <?php //if (Helper::checkRoute($controllerId . 'update') && $model->id_user != Yii::$app->user->id): ?>
                                <li>
                                    <?= Html::a(Yii::t('backend',
                                            'Update') . '<span class="pull-right badge bg-yellow"><i class="fa fa-pencil"></i></span>',
                                        ['update', 'id' => $model->id_user], [
                                        ]) ?>
                                </li>
                            <?php //endif; ?>
                            <?php if ($model->status == User::STATUS_DELETED && Yii::$app->user->id != $model->id_user): ?>
                                <li>
                                    <?= Html::a(Yii::t('backend',
                                            'Restore') . '<span class="pull-right badge bg-green"><i class="fa fa-check"></i></span>', [
                                        'restore',
                                        'id'          => $model->id_user,
                                        'callbackURL' => Yii::$app->request->url,
                                    ], [
                                        'data' => [
                                            'confirm' => Yii::t('backend', 'Are you sure you want to restore this item?'),
                                            'method'  => 'post',
                                        ],
                                    ]); ?>
                                </li>
                            <?php endif; ?>
                            <?php
                            if ($model->status == User::STATUS_ACTIVE && Helper::checkRoute($controllerId . 'delete') && Yii::$app->user->id != $model->id_user):?>
                                <li>
                                    <?= Html::a(Yii::t('backend',
                                            'Delete') . '<span class="pull-right badge bg-red"><i class="fa fa-remove"></i></span>',
                                        [
                                            'delete',
                                            'id'          => $model->id_user,
                                            'callbackURL' => Yii::$app->request->url,
                                        ],
                                        [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                                'method'  => 'post',
                                            ],
                                        ]
                                    ); ?>
                                </li>
                            <?php endif; ?>
                            <?php
                            if (Helper::checkRoute($controllerId . 'reset-password')) :?>
                                <li>
                                    <?= Html::a(Yii::t('backend',
                                            'Reset Password') . '<span class="pull-right badge bg-teal"><i class="fa fa-refresh"></i></span>', [
                                        'reset-password',
                                        'id' => $model->id_user,
                                    ],
                                        [
                                            'data' => [
                                                'confirm' => Yii::t('backend', 'Are you sure you want to reset this user password?'),
                                                'method'  => 'post',
                                            ],
                                        ]
                                    ); ?>
                                </li>
                            <?php endif;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <?= Yii::t('backend', 'Information') ?>
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model'      => $model,
                        'attributes' => [
                            'username',
                            'full_name',
                            'email:email',
                            [
                                'label'  => Yii::t('backend', 'Roles'),
                                'value'  => $model->displayRoles,
                                'format' => 'raw',
                            ],
                            'created_at:date',
                            [
                                'attribute' => 'status',
                                'value'     => User::statusLabel($model->status),
                            ],
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
