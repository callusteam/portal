<?php

/* @var $this yii\web\View */
/* @var $model backend\models\form\Avatar */

$this->title                   = Yii::t('backend', 'Change avatar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user->username, 'url' => ['view', 'id' => $model->user->id_user]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="user-update">
    
    <?= $this->render('_form-change-avatar', [
        'model' => $model,
    ]) ?>

</div>
