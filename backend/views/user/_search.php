<?php

use common\models\User;
use common\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <?= Yii::t('app', 'Search'); ?>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'username') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'full_name') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'status')->widget(Select2::class,
                        [
                            'data'          => User::statusLabels(),
                            'pluginOptions' => [
                                'placeholder' => Yii::t('backend', 'Select'),
                                'allowClear'  => true,
                            ],
                        ]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'email') ?>
                </div>
            </div>


            <div class="form-group">
                <?= Html::submitButton('<i class="fa fa-search" aria-hidden="true"></i> ' . Yii::t('backend', 'Search'), [
                    'class' => 'btn btn-primary
                btn-search',
                    'title' => Yii::t('backend', 'Search (Alt+F)'),
                ]) ?>
                <?= Html::a('<i class="fa fa-refresh" aria-hidden="true"></i> ' . Yii::t('backend', 'Refresh'), ['index'],
                    ['class' => 'btn btn-default']) ?>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
