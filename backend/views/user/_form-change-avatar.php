<?php

use bupy7\cropbox\CropboxWidget;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $model backend\models\form\Avatar */
?>
<div class="user-form">
    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'options' => ['class' => 'form'],
                ],
            ]); ?>
            
            
            <?=$form->field($model,'image_file')->widget(CropboxWidget::class,[
                'croppedDataAttribute' => 'crop_info',
            ]);?>


            <div class="form-group">
                <?=Html::submitButton('<i class="fa fa-floppy-o" aria-hidden="true"></i> ' . Yii::t('backend','Upload'),[
                    'class' => 'btn btn-primary btn-submit',
                    'title' => Yii::t('backend','Upload'),
                ])?>
                <?=Html::button(Yii::t('backend','Cancel'),['class' => 'btn btn-danger btn-back','title' => Yii::t('backend','Cancel')])?>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>