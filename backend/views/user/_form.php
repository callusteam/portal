<?php

use common\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\form\SignUp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <div class="box box-primary">

        <div class="box-body">
            <?php $form = ActiveForm::begin(['options' => ['class' => 'form'],]); ?>
            
            <?=$form->field($model,'username')->textInput([
                'maxlength'    => true,
                'autocomplete' => 'new-username',
            ])->label(Yii::t('backend','Username'))?>
            
            <?=$form->field($model,'full_name')->textInput()->label(Yii::t('backend','Full Name'))?>
            
            <?=$form->field($model,'email')->textInput()->label(Yii::t('backend','Email'))?>
    
            <?php echo $form->field($model,'roles')->widget(Select2::class,[
                'data'          => ArrayHelper::map(Yii::$app->authManager->getRoles(),'name','name'),
                'options'       => [
                    'multiple' => true,
                    'placeholder' => Yii::t('backend','Select'),
                ],
                'pluginOptions' => [
                    'allowClear'  => true,
                ],
            ])->label(Yii::t('backend','Roles'))?>

            
            <?=$form->field($model,'should_send_activation_mail')->checkbox(['label' => Yii::t('backend','Should Send Activation Mail')])?>
            
            <?=$form->field($model,'status')->dropDownList(User::statusLabels())->label(Yii::t('backend','Status'))?>

            <div class="form-group">
                <?=Html::submitButton(Yii::t('backend','Create'),[
                    'class' => 'btn btn-success btn-submit',
                    'title' => Yii::t('backend','Create'),
                ])?>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
