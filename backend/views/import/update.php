<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Import */

$this->title = Yii::t('backend', 'Update Import: {name}', [
    'name' => $model->id_import,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Imports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_import, 'url' => ['view', 'id' => $model->id_import]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="import-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
