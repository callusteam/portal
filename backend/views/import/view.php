<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Import */

$this->title                   = $model->id_import;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Imports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="import-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id_import], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id_import], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>
    
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id_import',
            'file_name',
            // 'file_path',
            // 'mapping:ntext',
            [
                'attribute' => 'type',
                'value'     => $model->typeDisplay,
            ],
            'log:ntext',
            [
                'attribute' => 'id_user',
                'value'     => $model->user->full_name,
            ],
            [
                'attribute' => 'status',
                'value'     => $model->statusDisplay,
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
