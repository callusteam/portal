<?php

use backend\models\Import;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ImportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="import-search">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <?= Yii::t('app', 'Search'); ?>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'id_import') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'type') ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->field($model, 'id_user') ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->field($model, 'status')->dropDownList(
                        Import::statusLabels(),
                        [
                            'prompt' => Yii::t('backend', 'All'),
                        ]
                    ) ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
