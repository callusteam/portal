<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Import */

$this->title = Yii::t('backend', 'Create Import');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Imports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
