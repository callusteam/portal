<?php

use common\models\Import;
use common\widgets\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Import */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="import-form">
    
    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'fileUpload')->widget(FileInput::class) ?>
    <?= $form->field($model, 'type')->dropDownList(Import::typeLabels()) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
