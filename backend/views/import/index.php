<?php

use common\widgets\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ImportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('backend', 'Imports');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-index">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'        => [
            'type'    => GridView::TYPE_PRIMARY,
            'heading' => '<i class="glyphicon glyphicon-list"></i> ' . $this->title,
        ],
        'toolbar'      => [
            [
                'content' =>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                        'data-pjax' => 0,
                        'class'     => 'btn btn-success',
                        'title'     => Yii::t('app', 'Add'),
                    ]) .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                        'data-pjax' => 0,
                        'class'     => 'btn btn-default',
                        'title'     => Yii::t('app', 'Làm mới'),
                    ]),
            ],
        ],
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'id_import',
            'file_name',
            // 'file_path',
            // 'mapping:ntext',
            [
                'attribute' => 'type',
                'value'     => 'typeDisplay',
            ],
            //'log:ntext',
            [
                'attribute' => 'id_user',
                'value'     => 'user.full_name',
            ],
            [
                'attribute' => 'status',
                'value'     => 'statusDisplay',
            ],
            'created_at:datetime',
            'updated_at:datetime',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
